package TreinamentoAppiumTurmaX;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        Assert.assertTrue( true );
    }

    @Test
    public void conectarAppiumEmuladorEAbrirCalculadora() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "9");
        caps.setCapability("deviceName", "Pixel 2 API 28");
        caps.setCapability("appPackage", "com.android.calculator2");
        caps.setCapability("appActivity", "com.android.calculator2.Calculator");

        AppiumDriver<MobileElement> driver;
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);

        Assert.assertTrue( true );
    }

    @Test
    public void abrirRelogio() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "9");
        caps.setCapability("deviceName", "Pixel 2 API 28");
        caps.setCapability("appPackage", "com.google.android.deskclock");
        caps.setCapability("appActivity", "com.android.deskclock.DeskClock");

        AppiumDriver<MobileElement> driver;
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
    }

    @Test
    public void retornaOAppPackageCorreto() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "9");
        caps.setCapability("deviceName", "Pixel 2 API 28");
        caps.setCapability("appPackage", "com.google.android.deskclock");
        caps.setCapability("appActivity", "com.android.deskclock.DeskClock");

        AppiumDriver<MobileElement> driver;
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);

        Assert.assertEquals("com.google.android.deskclock", driver.getCapabilities().getCapability("appPackage"));
    }

    @Test
    public void retornaOContextCorreto() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "9");
        caps.setCapability("deviceName", "Pixel 2 API 28");
        caps.setCapability("appPackage", "com.google.android.deskclock");
        caps.setCapability("appActivity", "com.android.deskclock.DeskClock");

        AppiumDriver<MobileElement> driver;
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);

        Assert.assertEquals("NATIVE_APP", driver.getContext());
    }

    @Test
    public void retornaOAppActivityCorreto() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "9");
        caps.setCapability("deviceName", "Pixel 2 API 28");
        caps.setCapability("appPackage", "com.google.android.deskclock");
        caps.setCapability("appActivity", "com.android.deskclock.DeskClock");

        AppiumDriver<MobileElement> driver;
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);

        Assert.assertEquals("com.android.deskclock.DeskClock", driver.getCapabilities().getCapability("appActivity"));
    }
}
