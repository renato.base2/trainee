using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class CasoTesteProjetoPAPage : PageBase
    {
        #region Mapping
        By titleTestCase = By.XPath("//div[contains(text(), 'Casos de Teste')]");
        By incluirButton = By.XPath("//button[@class='btn btn-crowdtest']");
        By tituloField = By.Id("name");
        By descriptionField = By.Id("description");
        By featureField = By.XPath("//input[@role='combobox']");
        By addFeature = By.XPath("//span[contains(text(),'Add item')]");
        By selectFeature = By.XPath("//span[contains(text(),'featureTeste')]");
        By expectedResultField = By.Id("expectedResult");
        By salvarButton = By.XPath("//button[@class='btn btn-crowdtest mr-1']");
        By messageSuccessPopUp = By.XPath("//p[contains(text(),'Caso de teste criado com sucesso.')]");
        By fecharButton = By.XPath("//button[contains(text(),'Fechar')]");
        #endregion

        #region Actions
        public void ClicarEmCasosDeTeste()
        {
            Click(titleTestCase);
        }

        public void ClicarEmIncluir()
        {
            Click(incluirButton);
        }

        public void PreencherTitulo(string name)
        {
            SendKeys(tituloField, name);
        }

        public void PreencherDescricao(string description)
        {
            SendKeys(descriptionField, description);
        }

        public void PreencherFeature(string feature)
        {
            SendKeys(featureField, feature);
            Click(addFeature);
            Refresh();
            Click(featureField);
            Click(selectFeature);
        }

        public void PreencherResultadoEsperado(string expectedResult)
        {
            SendKeys(expectedResultField, expectedResult);
        }

        public void ClicarEmSalvar()
        {
            Click(salvarButton);
        }

        public void ClicarEmFechar()
        {
            Click(fecharButton);
        }
        public string RetornaMensagemDeSucesso()
        {
            return GetText(messageSuccessPopUp);
        }
        #endregion
    }
}
