using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping
        By usernameField = By.Id("login");
        By passwordField = By.Id("password");
        By loginButton = By.XPath("//button[@class='btn btn-crowdtest btn-block']");
        By prosseguirButton = (By.XPath("//a[@class='cc-btn cc-dismiss']"));
        By mensagemErroTextArea = By.XPath("//span[@class='error-msg']");
        #endregion

        #region Actions
        public void ClicarEmProsseguir()
        {
            Click(prosseguirButton);
        }

        public void PreencherUsuario(string usuario)
        {
            SendKeys(usernameField, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(passwordField, senha);
        }

        public void ClicarEmLogin()
        {
            Click(loginButton);
        }

        public string RetornaMensagemDeErro()
        {
            return GetText(mensagemErroTextArea);
        }
        #endregion
    }
}
