using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class NovoProjetoPage : PageBase
    {
        #region Mapping
        By idField = By.Id("identifier");
        By nameField = By.Id("name");
        By descriptionField = By.Id("description");
        By typeField = By.XPath("//input[@role='combobox']");
        By typeSelection = By.XPath("//span[contains(text(),'Desktop')]");
        By salvarButton = By.XPath("//button[@class='btn btn-crowdtest mr-1']");
        By messageSuccessPopUp = By.XPath("//p[contains(text(),'Projeto criado com sucesso.')]");
        By fecharButton = By.XPath("//button[contains(text(),'Fechar')]");
        By voltarButton = By.XPath("//button[contains(text(),'Voltar')]");
        By deleteProject = By.XPath("//mat-cell[text() = 'projectName']//following-sibling::mat-cell[@class='mat-cell cdk-column-actions mat-column-actions ng-star-inserted']//*[@data-icon='trash-alt']");
        By confirmDeleteProject = By.XPath("//span[contains(text(),'Sim')]");
        #endregion

        #region Actions

        public void PreencherId(string identificador)
        {
            SendKeys(idField, identificador);
        }

        public void PreencherNome(string nome)
        {
            SendKeys(nameField, nome);
        }

        public void PreencherDescricao(string description)
        {
            SendKeys(descriptionField, description);
        }

        public void SelecionarTipo()
        {
            Click(typeField);
            Click(typeSelection);
        }

        public void ClicarEmSalvar()
        {
            Click(salvarButton);
        }

        public string RetornaMensagemDeSucesso()
        {
            return GetText(messageSuccessPopUp);
        }

        public void ClicarEmFechar()
        {
            Click(fecharButton);
        }

        public void ClicarEmVoltar()
        {
            Click(voltarButton);
        }

        public void DeletarProjetoTeste(string projectName)
        {
            Console.WriteLine(projectName);
            string nameProjetct = deleteProject.ToString().Replace("projectName",projectName).Replace("By.xpath: ","");;
            Console.WriteLine(nameProjetct);
            deleteProject = By.XPath(nameProjetct);
            Console.WriteLine(deleteProject);
            Click(deleteProject);
            Click(confirmDeleteProject);
        }
        #endregion
    }
}
