﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MainPage : PageBase
    {
        #region Mapping
        By usernameLoginInfoTextArea = By.XPath("//span[contains(text(),'Bem-vindo ao Crowdtest! O que deseja fazer hoje?')]");
        By gerenciarInfoTextArea = By.XPath("//span[contains(text(),'Selecione um projeto para visualizar os gráficos')]");
        By projetosInfoTextArea = By.XPath("//h1[contains(text(),'Projetos')]");
        #endregion

        #region Actions
        public string RetornaUsernameDasInformacoesDeLogin()
        {
            return GetText(usernameLoginInfoTextArea);
        }

        public string RetornaSolicitacaoParaSelecionarUmProjeto()
        {
            return GetText(gerenciarInfoTextArea);
        }

          public string RetornaPaginaDosProjetos()
        {
            return GetText(projetosInfoTextArea);
        }
        #endregion
    }
}
