using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class GerenciarPage : PageBase
    {
        #region Mapping
        By gerenciarButton = By.XPath("//button[@class='btn btn-crowdtest mr-1']");
 
        #endregion

        #region Actions
        public void ClicarEmGerenciar()
        {
            Click(gerenciarButton);
        }
        #endregion
    }
}
