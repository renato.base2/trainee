using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ProjetosPage : PageBase
    {
        #region Mapping
        By menuProjetos = By.XPath("//a[@href='/projects']");
        By casosDeTeste = By.XPath("//mat-cell[@class='mat-cell cdk-column-testCases mat-column-testCases ng-star-inserted']");
        By novoProjetoButton = By.XPath("//button[@class='btn btn-crowdtest']");
        By projetoPA = By.XPath("//mat-cell[contains(text(),'PA')]");
        #endregion

        #region Actions
        public void ClicarNoMenuProjetos()
        {
            Click(menuProjetos);
        }
        
        public string RetornaQuantidadeCasosTeste()
        {
            return GetText(casosDeTeste);
        }

        public void ClicarNovoProjeto()
        {
            Click(novoProjetoButton);
        }

        public void ClicarNoProjetoPA()
        {
            Click(projetoPA);
        }
        #endregion
    }
}