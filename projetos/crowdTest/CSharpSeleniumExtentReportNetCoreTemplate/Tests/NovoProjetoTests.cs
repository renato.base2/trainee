﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class NovoProjetoTests : TestBase
    {
        #region Pages and Flows Objects
        MainPage mainPage;
        LoginFlows loginFlows;
        GerenciarPage gerenciarPage;
        ProjetosPage projetosPage;
        NovoProjetoPage novoProjetoPage;
        #endregion

        [Test]
        public void CriarNovoProjetoComSucesso()
        {
            mainPage = new MainPage();
            loginFlows = new LoginFlows();
            gerenciarPage = new GerenciarPage();
            projetosPage = new ProjetosPage();
            novoProjetoPage = new NovoProjetoPage();

            #region Parameters
            string username = BuilderJson.ReturnParameterAppSettings("LOGIN_CROWDTEST");
            string password = BuilderJson.ReturnParameterAppSettings("SENHA_CROWDTEST");
            string identificador = "PA";
            string name = "Projeto Aldrich";
            string description = "Automatizar a criação de novo projeto e um caso de teste";
            string message = "criado com sucesso";
            #endregion

            loginFlows.EfetuarLogin(username, password);
            gerenciarPage.ClicarEmGerenciar();
            projetosPage.ClicarNoMenuProjetos();
            projetosPage.ClicarNovoProjeto();
            novoProjetoPage.PreencherId(identificador);
            novoProjetoPage.PreencherNome(name);
            novoProjetoPage.PreencherDescricao(description);
            novoProjetoPage.SelecionarTipo();
            novoProjetoPage.ClicarEmSalvar();
            
            Assert.IsTrue(novoProjetoPage.RetornaMensagemDeSucesso().Contains(message));

            novoProjetoPage.ClicarEmFechar();
            novoProjetoPage.ClicarEmVoltar();
            novoProjetoPage.DeletarProjetoTeste(name);
        }
    }
}
