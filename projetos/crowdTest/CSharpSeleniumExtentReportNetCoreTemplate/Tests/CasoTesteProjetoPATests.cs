﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CasoTesteProjetoPATests : TestBase
    {
        #region Pages and Flows Objects
        MainPage mainPage;
        LoginFlows loginFlows;
        GerenciarPage gerenciarPage;
        ProjetosPage projetosPage;
        NovoProjetoPage novoProjetoPage;
        CasoTesteProjetoPAPage casoTesteProjetoPAPage;
        #endregion

        [Test]
        public void CriarNovoCasoDeTesteComSucesso()
        {
            mainPage = new MainPage();
            loginFlows = new LoginFlows();
            gerenciarPage = new GerenciarPage();
            projetosPage = new ProjetosPage();
            novoProjetoPage = new NovoProjetoPage();
            casoTesteProjetoPAPage = new CasoTesteProjetoPAPage();

            #region Parameters
            string username = BuilderJson.ReturnParameterAppSettings("LOGIN_CROWDTEST");
            string password = BuilderJson.ReturnParameterAppSettings("SENHA_CROWDTEST");
            string identificador = "TestPA";
            string name = "Projeto Aldrich";
            string descriptionProject = "Automatizar a criação de novo projeto e um caso de teste";
            string title = "Cadastrar novo caso de teste com sucesso";
            string descriptionCaseTest = "Teste";
            string feature = "featureTeste";
            string expectedResult = "Pop Up informando que caso de teste foi criado com sucesso";
            string message = "criado com sucesso.";
            #endregion

            loginFlows.EfetuarLogin(username, password);
            gerenciarPage.ClicarEmGerenciar();
            projetosPage.ClicarNoMenuProjetos();
            projetosPage.ClicarNovoProjeto();
            novoProjetoPage.PreencherId(identificador);
            novoProjetoPage.PreencherNome(name);
            novoProjetoPage.PreencherDescricao(descriptionProject);
            novoProjetoPage.SelecionarTipo();
            novoProjetoPage.ClicarEmSalvar();
            novoProjetoPage.ClicarEmFechar();
            casoTesteProjetoPAPage.ClicarEmCasosDeTeste();
            casoTesteProjetoPAPage.ClicarEmIncluir();
            casoTesteProjetoPAPage.PreencherFeature(feature);
            casoTesteProjetoPAPage.PreencherTitulo(title);
            casoTesteProjetoPAPage.PreencherDescricao(descriptionCaseTest);
            casoTesteProjetoPAPage.PreencherResultadoEsperado(expectedResult);
            casoTesteProjetoPAPage.ClicarEmSalvar();

            Assert.IsTrue(casoTesteProjetoPAPage.RetornaMensagemDeSucesso().Contains(message));

            novoProjetoPage.ClicarEmFechar();
            projetosPage.ClicarNoMenuProjetos();
            novoProjetoPage.DeletarProjetoTeste(name);
        }
    }
}
