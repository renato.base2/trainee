﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class ProjetosTests : TestBase
    {
        #region Pages and Flows Objects
        MainPage mainPage;
        LoginFlows loginFlows;
        GerenciarPage gerenciarPage;
        ProjetosPage projetosPage;
        #endregion

        [Test]
        public void ClicarNoMenuProjetosComSucesso()
        {
            mainPage = new MainPage();
            loginFlows = new LoginFlows();
            gerenciarPage = new GerenciarPage();
            projetosPage = new ProjetosPage();

            #region Parameters
            string username = BuilderJson.ReturnParameterAppSettings("LOGIN_CROWDTEST");
            string password = BuilderJson.ReturnParameterAppSettings("SENHA_CROWDTEST");
            string message = "Projetos";
            #endregion

            loginFlows.EfetuarLogin(username, password);
            gerenciarPage.ClicarEmGerenciar();
            projetosPage.ClicarNoMenuProjetos();
            string quantidadeCasosTeste = projetosPage.RetornaQuantidadeCasosTeste();

            Assert.AreEqual(message, mainPage.RetornaPaginaDosProjetos());
            Assert.IsFalse(quantidadeCasosTeste.Equals("0"));
        }
    }
}
