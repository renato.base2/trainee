﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class GerenciarTests : TestBase
    {
        #region Pages and Flows Objects
        MainPage mainPage;
        LoginFlows loginFlows;
        GerenciarPage gerenciarPage;
        #endregion

        [Test]
        public void ClicarNoBotaoGerenciarComSucesso()
        {
            mainPage = new MainPage();
            loginFlows = new LoginFlows();
            gerenciarPage = new GerenciarPage();

            #region Parameters
            string username = BuilderJson.ReturnParameterAppSettings("LOGIN_CROWDTEST");
            string password = BuilderJson.ReturnParameterAppSettings("SENHA_CROWDTEST");
            string message = "Selecione um projeto para visualizar os gráficos";
            #endregion

            loginFlows.EfetuarLogin(username, password);
            gerenciarPage.ClicarEmGerenciar();

            Assert.AreEqual(message, mainPage.RetornaSolicitacaoParaSelecionarUmProjeto());
        }
    }
}
