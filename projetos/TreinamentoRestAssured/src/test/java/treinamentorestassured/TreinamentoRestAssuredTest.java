package treinamentorestassured;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.github.javafaker.Faker;
import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.lang.reflect.Method;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.baseURI;

public class TreinamentoRestAssuredTest {

    public static ExtentReports EXTENT_REPORT = null;

    public static ExtentHtmlReporter HTML_REPORTER = null;

    public static ExtentTest TEST;

    public static String reportPath = "target/surefire-reports/SuiteTestes";

    public static String fileName = "TreinamentoRestAssured.html";

    @DataProvider(name = "formatosDeEmailInvalidos")
    private Object[] dataUsuariosEmailsInvalidosProvider() {
        Faker fake = new Faker();

        Usuarios usuario1 = new Usuarios();
        usuario1.setNome(fake.name().fullName());
        usuario1.setEmail("@email.com");
        usuario1.setPassword("teste");
        usuario1.setAdministrador("true");

        Usuarios usuario2 = new Usuarios();
        usuario2.setNome(fake.name().fullName());
        usuario2.setEmail("testeemail.com");
        usuario2.setPassword("teste");
        usuario2.setAdministrador("true");

        Usuarios usuario3 = new Usuarios();
        usuario3.setNome(fake.name().fullName());
        usuario3.setEmail("test@email");
        usuario3.setPassword("teste");
        usuario3.setAdministrador("true");

        Usuarios usuario4 = new Usuarios();
        usuario4.setNome(fake.name().fullName());
        usuario4.setEmail("test@");
        usuario4.setPassword("teste");
        usuario4.setAdministrador("true");

        Usuarios usuario5 = new Usuarios();
        usuario5.setNome(fake.name().fullName());
        usuario5.setEmail("test@email.");
        usuario5.setPassword("teste");
        usuario5.setAdministrador("true");

        return new Usuarios[]{usuario1, usuario2, usuario3, usuario4, usuario5};
    }

    @DataProvider(name = "formatosDeEmailInvalidosCSV")
    public Iterator<Object []> datadataUsuariosEmailsInvalidosProviderCSV() {
        return csvProvider("src/test/java/treinamentorestassured/emailsInvalidos.csv");
   }

    public Iterator<Object []> csvProvider(String csvNamePath){
        String line = "";
        String cvsSplitBy = ";";
        List<Object []> testCases = new ArrayList<>();
        String[] data= null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvNamePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = line.split(cvsSplitBy);
            testCases.add(data);
        }
        return testCases.iterator();
    }

    private void base() {
    baseURI = "http://localhost";
    port = 3000;
    }

    private String login() {
        base();
        String token = given()
                .contentType(ContentType.JSON)
//                .auth().basic("fulano@qa.com", "teste")
                .body("{\n" +
                        "  \"email\": \"fulano@qa.com\",\n" +
                        "  \"password\": \"teste\"\n" +
                        "}")
                .when()
                .post("/login")
                .then()
                .statusCode(200)
                .extract()
                .path("authorization");
        System.out.println(token);
        return token;
    }

    private void cadastrarCarrinho() {
        base();
        given()
                .contentType(ContentType.JSON)
//                .header("Authorization", login())
                .auth().oauth2("$login()")
                .body("{\n" +
                        "  \"produtos\": [\n" +
                        "    {\n" +
                        "      \"idProduto\": \"BeeJh5lz3k6kSIzA\",\n" +
                        "      \"quantidade\": 1\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}")
                .when()
                .post("/carrinhos");
    }

    private Object criarUsuario() {
        Usuarios usuario = new Usuarios();
        Faker fake = new Faker();
        usuario.setNome(fake.name().fullName());
        usuario.setEmail(fake.internet().emailAddress());
        usuario.setPassword("teste");
        usuario.setAdministrador("true");
        return usuario;
    }

    @BeforeSuite
    public void beforSuite() {
        EXTENT_REPORT = new ExtentReports();
        HTML_REPORTER = new ExtentHtmlReporter(reportPath+"/"+fileName);
        EXTENT_REPORT.attachReporter(HTML_REPORTER);
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        TEST = EXTENT_REPORT.createTest(method.getName());
    }

    @AfterMethod
    public void afterTest(ITestResult result) {
        switch (result.getStatus())
        {
            case ITestResult.FAILURE:
                TEST.log(Status.FAIL, result.getThrowable().toString());
                break;
            case ITestResult.SKIP:
                TEST.log(Status.SKIP, result.getThrowable().toString());
                break;
            default:
                TEST.log(Status.PASS, "Sucesso");
                break;
    }
        }

    @Test
    public void teste_usuarios_pesquisarUsuarios() {
        base();
        given()
                .contentType(ContentType.JSON)
                .when()
                .get("/usuarios")
                .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .body(
//                        "usuarios", hasSize(1),
                        "$", hasKey("quantidade"),
                        "usuarios", hasItem(hasKey("_id"))
                );
    }

    @Test
    public void teste_usuarios_pesquisarUsuarioInexistente() {
        base();
        given()
                .pathParam("_id", "0uxuPY0cbmQhpEzB")
                .contentType(ContentType.JSON)
                .when()
                .get("/usuarios/{_id}")
                .then()
                .log().all()
                .assertThat()
                .statusCode(400)
                .body(
                        "message", equalTo("Usuário não encontrado")
                );
    }

    @Test
    public void teste_usuarios_pesquisarUsuarioPeloId() {
        base();
        given()
                .pathParam("_id", "0uxuPY0cbmQhpEz1")
                .contentType(ContentType.JSON)
                .when()
                .get("/usuarios/{_id}")
                .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .body(
                        "nome", equalTo("Fulano da Silva"),
                        "email", equalTo("fulano@qa.com"),
                        "password", equalTo("teste"),
                        "administrador", equalTo("true"),
                        "_id", equalTo("0uxuPY0cbmQhpEz1")
                );
    }

    @Test
    public void teste_usuarios_cadastrarNovoUsuario() {
        base();
        given()
                .contentType(ContentType.JSON)
                .body(criarUsuario())
                .when()
                .post("/usuarios")
                .then()
                .assertThat()
                .statusCode(201);
    }

    @Test(dataProvider = "formatosDeEmailInvalidos")
    public void teste_usuarios_cadastrarUsuarioComEmailsInvalidosDataDriven(Usuarios usuariosData) {
        base();
        given()
                .contentType(ContentType.JSON)
                .body(usuariosData)
                .when()
                .post("/usuarios")
                .then()
                .assertThat()
                .statusCode(400)
                .body(
                        "email", equalTo("email deve ser um email válido")
                );
    }

    @Test(dataProvider = "formatosDeEmailInvalidosCSV")
    public void teste_usuarios_cadastrarUsuarioComEmailsInvalidosDataDrivenCSV(String email, String password, String administrador) {
        Usuarios usuario = new Usuarios();
        Faker fake = new Faker();
        usuario.setNome(fake.name().fullName());
        usuario.setEmail(email);
        usuario.setPassword(password);
        usuario.setAdministrador(administrador);

        base();
        given()
                .log().all()
                .contentType(ContentType.JSON)
                .body(usuario)
                .when()
                .post("/usuarios")
                .then()
                .assertThat()
                .statusCode(400)
                .body(
                        "email", equalTo("email deve ser um email válido")
                );
    }

    @Test
    public void teste_usuarios_cadastrarUsuarioJaExistente() {
        base();
        given()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"nome\": \"Fulano da Silva\",\n" +
                        "  \"email\": \"fulano@qa.com\",\n" +
                        "  \"password\": \"teste\",\n" +
                        "  \"administrador\": \"true\"\n" +
                        "}")
                .when()
                .post("/usuarios")
                .then()
                .assertThat()
                .statusCode(400)
                .body("message", equalTo("Este email já está sendo usado"));
    }

    @Test
    public void teste_usuarios_loginComSucesso() {
        base();
        String token = given()
                .contentType(ContentType.JSON)
//                .auth().basic("fulano@qa.com", "teste")
                .body("{\n" +
                        "  \"email\": \"fulano@qa.com\",\n" +
                        "  \"password\": \"teste\"\n" +
                        "}")
                .when()
                .post("/login")
                .then()
                .log().all()
                .statusCode(200)
                .body("message", equalTo("Login realizado com sucesso"),
                        "authorization", not(empty())
                )
                .extract()
                .path("authorization");
    }

    @Test
    public void teste_usuarios_pesquisarUsuariosMetodoInvalido() {
        base();
        given()
                .contentType(ContentType.JSON)
                .when()
                .options("/usuarios")
                .then()
                .log().all()
                .assertThat()
                .statusCode(204);
    }

    @Test
    public void teste_carrinhos_cadastrarCarrinhoComSucesso() {
        base();
        given()
                .contentType(ContentType.JSON)
                .header("Authorization", login())
//                .auth().oauth2("$login()")
                .body("{\n" +
                        "  \"produtos\": [\n" +
                        "    {\n" +
                        "      \"idProduto\": \"BeeJh5lz3k6kSIzA\",\n" +
                        "      \"quantidade\": 1\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}")
                .when()
                .post("/carrinhos")
                .then()
                .statusCode(201)
                .body(
                        "message", equalTo("Cadastro realizado com sucesso"),
                        "_id", not(empty())
                );
    }

    @Test
    public void teste_carrinhos_cancelarCompra() {
        base();
        given()
                .header("Authorization", login())
                .when()
                .delete("/carrinhos/cancelar-compra")
                .then()
                .assertThat()
                .statusCode(200)
                .body(
                        "message", equalTo("Registro excluído com sucesso. Estoque dos produtos reabastecido")
                );


    }

//    @Test
//    public void teste_produtos_cadastrarProdutoComSucesso() {
//        JSONObject produto = new JSONObject();
//        produto.put("nome", "Combo Multilaser C202");
//        produto.put("preco", 125);
//        produto.put("descricao", "Combo teclado e mouse");
//        produto.put("quantidade", 1);
//
//        base();
//        given()
//                .log().all()
////                .auth().oauth2("$login()")
//                .header("Authorization", login())
//                .body(produto)
//                .when()
//                .post("/produtos")
//                .then()
//                .assertThat()
//                .statusCode(201)
//                .body(
//                        "message", equalTo("Cadastro realizado com sucesso")
//                );
//    }
//
//    @Test
//    public void teste_produtos_editarProdutoComSucesso() {
//        base();
//        given()
//                .header("Authorization", login())
//                .body("{\n" +
//                        "  \"nome\": \"Logitech MX Vertical\",\n" +
//                        "  \"preco\": 470,\n" +
//                        "  \"descricao\": \"Mouse\",\n" +
//                        "  \"quantidade\": 385\n" +
//                        "}")
//                .when()
//                .put("/produtos/0uxuPY0cbmQhpEz1")
//                .then()
//                .assertThat()
//                .statusCode(200)
//                .body(
//                        "message", equalTo("Registro alterado com sucesso")
//                );
//    }

    @AfterSuite
    public void afterSuite(){
        EXTENT_REPORT.flush();
    }
}
